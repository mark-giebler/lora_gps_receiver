// -*- mode: C++ -*-
// Example sketch showing how to create a simple messaging client (receiver)
// with the RH_RF95 class. RH_RF95 class does not provide for addressing or
// reliability, so you should only use RH_RF95 if you do not need the higher
// level messaging abilities.
// It is designed to work with the other example Feather9x_TX
//
// LoRa_GPS_Receiver
// Giebler
// This code receives data sent from the LoRa_GPS_Tracker project.
// Displays signal strength and GPS data on the OLED display
// as well as sending it:
//	- out the serial port.
//	- to an MQTT broker via WiFi connection
//
//
// IDE: Arduino 1.8.16
// IDE Board setting:
// 	    	 ESP32 Arduino --> "Heltec Wifi Lora 32" LoRa + OLED Board
// or		 ESP32 Arduino --> "Heltec Wifi Lora 32 (V2)" Marked on bottom of board: WiFi_LoRa_32_V2
// or Heltec ESP32 Arduino --> "WiFi LoRa 32"  (this build target selection is untested yet)
//
//		   ESP32 Arduino BSP: https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_dev_index.json
//	Heltec ESP32 Arduino BSP: https://resource.heltec.cn/download/package_heltec_esp32_index.json
//
//	Board Info: https://heltec.org/project/wifi-lora-32/
//
// This code uses the RH_RF95 library to control the LoRa xcvr instead of Heltec's library.
// This was done because the LoRa_GPS_Tracker transmitter uses an Adafruit M0+ LoRa board which uses the
// RH_RF95 library and it was easier to get the RF link parameters (data rate, modulation width, etc)
// to match using the same library.
//
// *  Our LoRa packet format:
// *  $<GPS Fix 0:1>,<Lat Deg>,<Long Deg>,<Vector speed>,<Vector angle>,<Altitude>,<Sat Count>,<V Battery>,<packet count>
// *  e.g.  $1,42.00877319,49.28430151,0.10,97.20,32.50,10,3.89,26098
//
// MQTT Info:
//	publish: mao/vbatt
//	publish: mao/lat
//	publish: mao/lon
//  publish: maogeo
//
// Node-red:
//		How To install:
//			https://randomnerdtutorials.com/getting-started-with-node-red-on-raspberry-pi/
//		Edit URL:
//			http://<Your_RPi_IP_address>:1880
//
// IoT data node-red dashboard:
//  MQTT test tool App for Chrome: MQTT Lens : https://chrome.google.com/webstore/detail/mqttlens/hemojaaeigabkbcookmlgmdigohjobjm
//  https://oneguyoneblog.com/2017/06/20/mosquitto-mqtt-node-red-raspberry-pi/
//  https://flows.nodered.org/node/node-red-dashboard
//  https://randomnerdtutorials.com/getting-started-with-node-red-dashboard/
//		Install dashboard at command line in ~/.node-red directory.
//			node-red-stop
//			npm install node-red-dashboard
//			reboot
//		Dashboard URL:
//			http://<Your_RPi_IP_address>:1880/ui
//
//
//	Android MQTT dashboard: https://play.google.com/store/apps/details?id=com.thn.iotmqttdashboard
//		Using Android mqtt dashboard example:
//			http://www.iotsharing.com/2017/05/how-to-use-mqtt-to-build-smart-home-arduino-esp32.html
//
// Node-Red world map dashboard:
// 	https://flows.nodered.org/node/node-red-contrib-web-worldmap
// 	Example using it: https://primalcortex.wordpress.com/2017/04/06/using-node-red-worldmap/
//	Installed within the nodeRed Web GUI, Three Bars Menu -> Manage palette -> search for "node-red-contrib-web-worldmap"
// 	Google API examples:
// 		https://flows.nodered.org/flow/452bba111f323218d3e17ee6eaa93e9c
// 		https://github.com/phyunsj/node-red-custom-dashboard-map-page
//
//	Worldmap URL:
//		http://<Your_RPi_IP_address>:1880/worldmap
//
//	Our Node-Red worldmap flows are in the file: flows.json
//
// MQTT client on ESP32 to Rpi:
// 	https://randomnerdtutorials.com/esp32-mqtt-publish-subscribe-arduino-ide/
//		RPi MQTT broker server port:
//			<Your_RPi_IP_address>:1883
//
// To AWS:
// 	https://www.valvers.com/open-software/arduino/esp32-mqtt-tutorial/
//  https://learn.sparkfun.com/tutorials/introduction-to-mqtt/all
//
// MQTT libraies:
// 	Aurduinomqtt by Kovelenta  (in IDE list)
// 	Or this very popular one: https://github.com/knolleary/pubsubclient/archive/master.zip (We are using this)
//
// JSON library:
//	Very popular: https://arduinojson.org/v6/example/generator/

#include <SPI.h>
#include <RH_RF95.h>

#include "heltec.h"

#include <WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

/* for feather m0 RFM9x
#define RFM95_CS	8
#define RFM95_RST	4
#define RFM95_INT	3
*/

/* Feather m0 w/wing
#define RFM95_RST	11		// "A"
#define RFM95_CS	10		// "B"
#define RFM95_INT	6		// "D"
*/

// HelTec Radio pins
#define LORA_DEFAULT_RESET_PIN	14
#define LORA_DEFAULT_SS_PIN		18
#define LORA_DEFAULT_DIO0_PIN	26

#define PA_OUTPUT_PA_BOOST_PIN	1
#define PA_OUTPUT_RFO_PIN		0


#if  defined( WIFI_LoRa_32 ) || defined( WIFI_LoRa_32_V2 )

	#define RFM95_RST	LORA_DEFAULT_RESET_PIN
	#define RFM95_CS	LORA_DEFAULT_SS_PIN
	#define RFM95_INT	LORA_DEFAULT_DIO0_PIN

#elif defined(ESP8266)
	/* for ESP w/featherwing */
	#define RFM95_CS	2	// "E"
	#define RFM95_RST	16	// "D"
	#define RFM95_INT	15	// "B"

#elif defined(ESP32)
	/* ESP32 feather w/wing */
	#define RFM95_RST	27	// "A"
	#define RFM95_CS	33	// "B"
	#define RFM95_INT	12	//  next to A

#elif defined(NRF52)
	/* nRF52832 feather w/wing */
	#define RFM95_RST	7	// "A"
	#define RFM95_CS	11	// "B"
	#define RFM95_INT	31	// "C"

#elif defined(TEENSYDUINO)
	/* Teensy 3.x w/wing */
	#define RFM95_RST	9	// "A"
	#define RFM95_CS	10	// "B"
	#define RFM95_INT	4	// "C"
#endif



// Change to frequency, must match RX's freq!
#define RF95_FREQ 912.0

// Singleton instance of the radio driver
RH_RF95 rf95(RFM95_CS, RFM95_INT);

// Blinky on LoRa receipt
//#define LED	13  // M0 LED
#define LED		25	// Heltec LED

String s_rssi = "RSSI --";
String s_freq = "Freq ------";

/* ------ MQTT Config ------------*/
/* include our ssid-password */
#include "___mySecrets.h"
const char* ssid = WIFI_SSID;
const char* password = WIFI_PSWD;

/* IP/Hostname of raspberry MQTT Server */
const char* mqtt_server = "pi-server";	// "raspberrypi-mg2";

/* create an instance of PubSubClient client */
WiFiClient espClient;
PubSubClient client(espClient);

/* Declare our topics we will publish.
 * Note the '/' character in a topic name causes trouble for some node red components. Todo: change to remove the '/' */
const char *VBATT_TOPIC	=	"mao/vbatt";
const char *RSSI_TOPIC	=	"mao/rssi";	/* RSSI dBm */
const char *SAT_FIX_TOPIC=	"mao/fix";	/* 1 = have GPS fix, 0 no fix */
const char *LAT_TOPIC	=	"mao/lat";
const char *LON_TOPIC	=	"mao/lon";
const char *SAT_TOPIC	=	"mao/sats";	/* number of satellites */
const char *GEO_TOPIC	=	"maogeo";	/* lat, long, and alt */
const char *HTBT_TOPIC	=	"devhb";	/* device heartbeat */
const char *mqttID 		=	"MaoTracker";  // Name of our device, must be unique
/* Declare the topics to which we will subscribe.
 */
const char *LED_TOPIC	=	"mao/led";	/* 1=on, 0=off */


// Global vars that get updated when a LoRa payload is received and parsed.
int sat_fix = -1;
int last_sat_fix = 0;
int last_rssi = 0;
int rssi = -1;
int last_vbatt = 0;
int vbatt = -1;
char s_lon[15];
char s_lat[15];
char s_alt[8];
char s_vecSpeed[6];
char s_vecAngle[6];
int alt = 0;
int last_alt = 0;
int sat_count = 0;
int last_sat_count = 0;
char s_vbatt[5];
uint32_t last_loraRx = 0;	// elapse time since last RX via LoRa.


/* MQTT RX callback.
 * Use for connectivity testing. RPi can toggle our LED
 * We accept any topic name (ignored)
 * If first char of payload is '1', LED ON, if '0' LED OFF.
 */
void receivedCallback(char* topic, byte* payload, unsigned int length) {
	Serial.print("MQTT rcvd: ");
	Serial.print(topic);

	Serial.print("  ::payload: ");
	for (int i = 0; i < length; i++) {
		Serial.print((char)payload[i]);
	}
	Serial.println();
	/* we got '1' -> on */
	if ((char)payload[0] == '1') {
		digitalWrite(LED_BUILTIN, HIGH);
	} else if((char)payload[0] == '0') {
		/* we got '0' -> on */
		digitalWrite(LED_BUILTIN, LOW);
	}
}

void mqttconnect() {
	/* Loop until reconnected */
	while (!client.connected()) {
		if(WiFi.status() != WL_CONNECTED)
			Serial.print("No WiFi ...");
		Serial.print("MQTT connecting ...");

		/* connect now with our unique client ID */
		if (client.connect(mqttID)) {
			Serial.println("connected");
			/* subscribe topic with default QoS 0*/
			client.subscribe(LED_TOPIC);
		} else {
			Serial.print("failed, status code =");
			Serial.print(client.state());
			Serial.println(" try again in 8 seconds");
			/* Wait 8 seconds before retrying */
			delay(8000);
		}
	}
}

/* -----  END MQTT -------- */
/*
 * Display LoRa packet on OLED
 * Resolution: 128x64
 * API info: https://github.com/HelTecAutomation/Heltec_ESP32/blob/master/src/oled/OLEDDisplay.h
 */
void OLED_LoRaData(char* rxPacket)
{
	String packet = rxPacket;
	Heltec.display->clear();
	Heltec.display->setTextAlignment(TEXT_ALIGN_LEFT);
	Heltec.display->setFont(ArialMT_Plain_10);
	Heltec.display->drawStringMaxWidth(0 , 15 , 128, packet);
	Heltec.display->drawString(0, 0, s_rssi);
	Heltec.display->drawString(50, 0, s_freq);
	Heltec.display->display();
}

const char spinChars[] = {"-/|\\"};
char statusIndicator[2] = {0,0};
/*!
 * update a status indicator character on the OLED.
 * Displayed in the upper right corner.
 */
void updateStatusIndicator(char sc)
{
	// erase previous char
	Heltec.display->setColor(BLACK);
	Heltec.display->drawString(128-7, 0, statusIndicator);
	// draw new char
	statusIndicator[0] = sc;
	Heltec.display->setColor(WHITE);
	Heltec.display->drawString(128-7, 0, statusIndicator);
	Heltec.display->display();
}

const char *tkn_sep = "$,";
/*
 * parse tokens out of string.
 * token separators listed in tkn_sep[]
 * Output: updates various global variables.
 * Input string is modified in parsing process.
 *
 * expected input format:
 *
 *  Our LoRa packet format:
 *  $,<GPS Fix 0:1>,<Lat Deg>,<Long Deg>,<Vector speed>,<Vector angle>,<Altitude>,<Sat Count>,<V Battery>,<packet count>
 *
 */
void parseTokens(char* in_str)
{
	char *p_tkn;

	if(*in_str != '$')
		return;
	in_str++;
	Serial.println("+++ Tokens +++");

	p_tkn = strtok (in_str, tkn_sep);	// get first token
	if( p_tkn == NULL )
		return ;
	// first token is fix
	sat_fix = atoi(p_tkn);
	Serial.print(String(p_tkn) + String(" "));

	p_tkn = strtok(NULL, tkn_sep);		// get next token
	strcpy(s_lat, p_tkn);
	Serial.print(String(p_tkn) + String(" "));

	p_tkn = strtok(NULL, tkn_sep);		// get next token
	strcpy(s_lon, p_tkn);
	Serial.print(String(p_tkn) + String(" "));

	p_tkn = strtok(NULL, tkn_sep);		// get next token
	strcpy(s_vecSpeed, p_tkn);
	Serial.print(String(p_tkn) + String(" "));

	p_tkn = strtok(NULL, tkn_sep);		// get next token
	strcpy(s_vecAngle, p_tkn);
	Serial.print(String(p_tkn) + String(" "));

	p_tkn = strtok(NULL, tkn_sep);		// get next token
	strcpy(s_alt, p_tkn);
	alt = atoi(p_tkn);
	Serial.print(String(p_tkn) + String(" "));

	p_tkn = strtok(NULL, tkn_sep);		// get next token
	sat_count = atoi(p_tkn);
	Serial.print(String(p_tkn) + String(" "));

	p_tkn = strtok(NULL, tkn_sep);		// get next token
	strcpy(s_vbatt, p_tkn);
	Serial.print(String(p_tkn) + String(" "));

	Serial.println();
	Serial.println("--- Tokens ---");
}

/* **********************************************************
 * Setup
 * **********************************************************/
void setup()
{
	s_lon[0] = 0;
	s_lat[0] = 0;
	s_alt[0] = 0;
	s_vecSpeed[0] = 0;
	s_vecAngle[0] = 0;
	s_vbatt[0] = 0;

	//WIFI Kit series V1 does not support Vext control
	Heltec.begin(true /*DisplayEnable Enable*/, false /*Heltec.Heltec.Heltec.LoRa Disable*/, false /*Serial Disable*/, true /*PABOOST Enable*/, ((long)RF95_FREQ*1E6) /*long BAND*/);

	pinMode(LED, OUTPUT);
	pinMode(RFM95_RST, OUTPUT);
	digitalWrite(RFM95_RST, HIGH);

	Serial.begin(230400);
	while (!Serial) {
			delay(1);
	}
	delay(100);
	Heltec.display->init();
	Heltec.display->flipScreenVertically();
	Heltec.display->setFont(ArialMT_Plain_10);
	Heltec.display->setColor(WHITE);
	Heltec.display->clear();

//	Heltec.display->drawString(0, 0, "Heltec.LoRa Init success!");
	Heltec.display->drawString(0, 10, "Connecting WiFi...");
	Heltec.display->display();

	// identify the sketch file in case we forget which one the binary was from.
	Serial.println("Sketch: \n" __FILE__ "\n");

	Serial.println("Heltec LoRa GPS receiver");
	// We start by connecting to a WiFi network
	Serial.println();
	Serial.print("WiFi Connecting to: ");
	Serial.println(ssid);

	WiFi.begin(ssid, password);
	delay(500);
	while (WiFi.status() != WL_CONNECTED) {
		delay(600);
		Serial.print(".");
	}
	Serial.println("");
	Serial.print("WiFi Connected: ");
	Serial.print("IP address: ");
	Serial.println(WiFi.localIP());
	// clear msg line from display
	Heltec.display->setColor(BLACK);
	Heltec.display->drawString(0, 10, "Connecting WiFi...");
	Heltec.display->display();
	Heltec.display->setColor(WHITE);

	/* configure the MQTT server with IPaddress and port */
	client.setServer(mqtt_server, 1883);
	/* this receivedCallback function will be invoked
	when client received subscribed topic */
	client.setCallback(receivedCallback);


	// manual reset
	digitalWrite(RFM95_RST, LOW);
	delay(10);
	digitalWrite(RFM95_RST, HIGH);
	delay(10);

	while (!rf95.init()) {
		Serial.println("LoRa radio init failed");
		Serial.println("Uncomment '#define SERIAL_DEBUG' in RH_RF95.cpp for detailed debug info");
		while (1);
	}
	Serial.println("LoRa radio init OK!");

	// Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM
	if (!rf95.setFrequency(RF95_FREQ)) {
		Serial.println("setFrequency failed");
		while (1);
	}
	Serial.print("Set Freq to: "); Serial.println(RF95_FREQ);
	long l_freq = ((long)(RF95_FREQ*1E3));
	s_freq = "Freq " + String(l_freq, DEC);
	// Defaults after init are 434.0MHz, 13dBm, Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol (7), CRC on

	// The default transmitter power is 13dBm, using PA_BOOST.
	// If you are using RFM95/96/97/98 modules which uses the PA_BOOST transmitter pin, then
	// you can set transmitter powers from 5 to 23 dBm:
	rf95.setTxPower(23, false);
	// show config:
	// dumpRegisters(Serial);
	Serial.println("+++ LoRa Radio Registers +++");
	LoRa.dumpRegisters(Serial);
	Serial.println("--- LoRa Radio Registers ---");

	Heltec.display->clear();
	Heltec.display->drawString(0, 0, "Heltec.LoRa Init success!");
	Heltec.display->drawString(0, 10, "Wait for incoming data...");
	Heltec.display->display();

	Serial.println("Wait for incoming data...");
	delay(1000);
}

/* **********************************************************
 * The Loop
 *
 * Here we monitor the LoRa radio for data to update
 * the MQTT brocker.
 *
 * **********************************************************/
void loop()
{

	static uint32_t next_update=0;	// next time to update MQTT data
	static uint32_t next_status=0;	// next time to update spinning status indicator.
	if(rf95.available())
	{
		// Should be a message for us now
		uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
		uint8_t len = sizeof(buf);

		if (rf95.recv(buf, &len))
		{
			digitalWrite(LED, HIGH);
			buf[len]=0;
			last_loraRx = millis();
#if DEBUG_LORA
			Serial.print("Len: "); Serial.println(len, DEC);
			RH_RF95::printBuffer("Received: ", buf, len);
			Serial.print("Got: [");
			Serial.print((char*)buf);
			Serial.println("]");
			Serial.print("RSSI: ");
			Serial.println(rf95.lastRssi(), DEC);
#else
			Serial.print((char*)buf);
			rssi = rf95.lastRssi();
			s_rssi = "RSSI: " + String(rssi, DEC);
			Serial.print(",");
			Serial.println(s_rssi);
			// Display rcvd info on OLED
			OLED_LoRaData((char*)buf);
#endif
			// parse the packet and save data to global vars (parsing destroys the buf[] )
			parseTokens((char*)buf);

			// Send a reply
			uint8_t data[] = "Ack!";
			rf95.send(data, sizeof(data));
			rf95.waitPacketSent();
#if DEBUG_LORA
			Serial.println("Sent a reply");
#endif
			digitalWrite(LED, LOW);
		}
		else
		{
			Serial.println("Receive failed");
		}
	}
	/* ---- Send via MQTT ----- */
	/* FIXME: _mg_ after running several days, the code below hangs in mqttconnect() ... */
	/* Can hang here if it can't connect to MQTT broker (maybe WiFi connection dropped?? no wfi ok.) */
	/* if client was disconnected then try to reconnect again */
	if(!client.connected()) {
		Heltec.display->drawString(0, 20, "Wait for MQTT ...");
		Heltec.display->display();
		mqttconnect();
		Heltec.display->setColor(BLACK);
		Heltec.display->drawString(0, 20, "Wait for MQTT ...");
		Heltec.display->setColor(WHITE);
		Heltec.display->display();
	}
	/* this function will listen for incomming
	subscribed topic-process-invoke receivedCallback */
	client.loop();

	if(millis() > next_update )
	{
		// time to send updates via MQTT
		int updates=0;
		Serial.print("MQTT update...");
		// indicate on OLED starting a MQTT update
		updateStatusIndicator('Q');
		next_status += 1024;

		if( rssi != last_rssi )
		{
			client.publish(RSSI_TOPIC, String(rssi, DEC).c_str());
			last_rssi = rssi;
			updates++;
		}
		if( sat_fix != last_sat_fix )
		{
			client.publish(SAT_FIX_TOPIC, String(sat_fix, DEC).c_str());
			last_sat_fix = sat_fix;
			updates++;
		}
		if( sat_count != last_sat_count )
		{
			client.publish(SAT_TOPIC, String(sat_count, DEC).c_str());
			last_sat_count = sat_count;
			updates++;
		}
		if(*s_vbatt != 0 )
		{
			client.publish(VBATT_TOPIC, s_vbatt);
			updates++;
		}

		if(*s_lat != 0 && *s_lat != '0' && *s_lon != 0 && *s_lon != '0')
		{
			client.publish(LAT_TOPIC, s_lat);
			client.publish(LON_TOPIC, s_lon);
			updates++;
			// setup JSON doc on stack.
			StaticJsonDocument<200> doc;
			// add latitude/longitude/... to json doc
			// similar format as Adafruit web IOT API: https://learn.adafruit.com/adafruit-io/mqtt-api
			doc["value"] = sat_fix;	// position fix or not.
			doc["lat"] = s_lat;
			doc["lon"] = s_lon;
			doc["ele"] = s_alt;
			char loc_json[192];
			serializeJson(doc, loc_json);
			client.publish(GEO_TOPIC, loc_json);
			updates++;
		}
		// publish a heartbeat
		char s_num[16];
		itoa(next_update, s_num, 10);
		client.publish(HTBT_TOPIC, s_num);
		updates++;

		Serial.println(updates);
		next_update = millis() + 15000ul;
	}

	if(millis() > next_status )
	{
		// bits 10 and 11 used to index spin char.
		next_status += 1024;
		updateStatusIndicator(spinChars[(next_status>>10)&0x03]);
	}
}
