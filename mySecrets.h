/*
 * This is just a template file for putting in git.
 * rename file with three underscore prefix (___mySecrets.h)
 * such that it is ignored by git (see .gitignore)
 * 
 * 
 * the WiFi secrets are defined here 
 */
#ifndef __MYSECRETS__
#define __MYSECRETS__

#define WIFI_SSID	"dd-wrt"
#define WIFI_PSWD	"00000000000"

#endif
